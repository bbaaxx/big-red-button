# Big Red Button:  A Webpack 2 super bloated example

### Because I am a big fan of Rube Goldberg

This repository contains a webpack2 configuration that includes ... well a large
stack that I have been able to wire together to achieve the task of displaying
a big red button (See dramatic screen shot below). The button only exists to be
clicked, but you will have to decide what to do when that happens.

## What you get? ##

* A big red button.
* Webpack2/HMR.
* ES6/2017/next/over9k/whatever via Babel.
* Sadistic eslint/flow setup (who's my little masoch?).
* Jest for that freggin test ...
* Sugarss/SASS/cssnext support out of the box ... simultaneously (with caveats).
* Dockerfile that runs flow... a pain in the buttocks to get to work.
* Material Design Lite.

## How to set it up? ##

1. Use a Linux or at least Mac operating systems.
2. Install [Git](https://git-scm.com/)
3. Then install [Yarn](https://yarnpkg.com)
4. Now, clone this repo `$ git clone <this repo>`
5. Go and gently `$ cd <yourself in there>`.
6. Install stuff running `$ yarn`
7. Please use one of these (I use [Atom](https://atom.io/)): `$ vim .`, `$ atom .`, `$ subl .`
8. Run `$ yarn dev` (you can even add `-- --open` for a super lazy combo).
9. Rejoice.

### Full feature list ###

(coming soon)

### Contribution guidelines ###

Please submit your PRs if you can add something or make something
work better.

### Who do I talk to? ###

* A discord server/channel thing will be available soon.

### License ###
MIT
